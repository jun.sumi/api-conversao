FROM openjdk:8-jdk-alpine
RUN addgroup -S apiconversao && adduser -S apiconversao -G apiconversao
USER apiconversao:apiconversao
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]