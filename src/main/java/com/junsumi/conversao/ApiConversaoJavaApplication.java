package com.junsumi.conversao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiConversaoJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiConversaoJavaApplication.class, args);
	}

}
