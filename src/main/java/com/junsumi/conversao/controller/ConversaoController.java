package com.junsumi.conversao.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.junsumi.conversao.controller.dto.ConvertDto;

@RestController
@RequestMapping("/")
public class ConversaoController {

	@GetMapping("/fahrenheit/{value}/celsius")
	public ResponseEntity<ConvertDto> convertFahrenheitToCelsius(@PathVariable("value") Double value) {
		return ResponseEntity.ok(ConvertDto.builder()
				.originalType("fahrenheit")
				.convertedType("celsius")
				.originalValue(value)
				.convertedValue((value - 32) * 5 / 9)
				.build());
	}
	
	@GetMapping("/celsius/{value}/fahrenheit")
	public ResponseEntity<ConvertDto> convertCelsiusToFahrenheit(@PathVariable("value") Double value) {
		return ResponseEntity.ok(ConvertDto.builder()
				.originalType("celsius")
				.convertedType("fahrenheit")
				.originalValue(value)
				.convertedValue((value * 9 / 5) + 32)
				.build());
	}
	
}
