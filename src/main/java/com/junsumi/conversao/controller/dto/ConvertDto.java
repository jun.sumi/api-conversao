package com.junsumi.conversao.controller.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ConvertDto {

	private String originalType;
	private String convertedType;
	private Double originalValue;
	private Double convertedValue;
	
}
